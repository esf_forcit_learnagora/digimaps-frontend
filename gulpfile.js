var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var build = require('gulp-build');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var replace = require('gulp-replace');

/* SASS TASKS */

gulp.task('sass:compile', function () {
    gulp.src(['sass/**/*.scss'])
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('app/css'));

});

gulp.task('sass:watch', function () {
    gulp.watch(['sass/**/*.scss'], ['sass:compile']);
});

gulp.task('compress', function() {
    gulp.src('app/scripts/**/*.js')
        .pipe(minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest('dist/js'))
});

gulp.task('build', function(){
    gulp.src('app/scripts/app.js')
        //.pipe(replace(/views\//g, '/bundles/angular/views/'))
        .pipe(gulp.dest('dist/scripts/'));
    gulp.src('app/*.html')
        .pipe(useref())
        //.pipe(replace(/scripts\/app.js/, 'bundles/angular/scripts/app.js'))
        //.pipe(gulpIf('*.js', uglify()))
        .pipe(gulp.dest('dist/'));
    gulp.src('app/views/**/*.html')
        .pipe(gulp.dest('dist/views'));
    gulp.src('app/img/**')
        .pipe(gulp.dest('dist/img'));
    gulp.src('app/bower_components/bootstrap/fonts/*')
        .pipe(gulp.dest('dist/fonts'));
    gulp.src('app/bower_components/font-awesome/fonts/*')
        .pipe(gulp.dest('dist/fonts'));
    gulp.src('app/fonts/*')
        .pipe(gulp.dest('dist/fonts'));
});

/* ASSETS TASKS */

gulp.task('default', ['sass:compile']);