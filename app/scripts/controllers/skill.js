'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MyJobCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('SkillCtrl', ['$rootScope','$scope', '$routeParams', 'Profile', 'Skill', 'Suggester', function ($rootScope, $scope, $routeParams, Profile, Skill, Suggester) {

        $rootScope.application.showMenu = false;
        $scope.profile = $rootScope.digimaps.profile;

        var skillId = $routeParams.id;
        var profile = $rootScope.digimaps.profile;
        var skill = null;
        _.each(profile.goals, function(goal) {
            var jobSkill = _.find(goal.job.job_skills, function(jobSkill) {
                return jobSkill.skill.id == skillId;
            });
            if (jobSkill) { skill = jobSkill.skill}
            console.log(jobSkill);
        });
        if (skill) {
            $scope.skill = skill;
        }

        $scope.testStarted = function(skill) {
            return skill.score.max_score > 0;
        };
        $scope.testInProgress = function(skill) {
            return skill.score.max_score > 0 && skill.score.max_score < skill.score.total_questions
        };
        $scope.testFinished = function(skill) {
            return skill.score.max_score == skill.score.total_questions;
        };

        $scope.reset = function() {
            console.log('will reset');
            Skill.resetScore(skillId).then( function(response) {
                $scope.lastTest = response.score;
                $scope.answers = response.answers;
                Profile.load().then(function(profile) {
                    var skill = null;
                    _.each(profile.goals, function(goal) {
                        var jobSkill = _.find(goal.job.job_skills, function(jobSkill) {
                            return jobSkill.skill.id == skillId;
                        });
                        if (jobSkill) { skill = jobSkill.skill}
                        console.log(jobSkill);
                    });
                    if (skill) {
                        $scope.skill = skill;
                    }
                });
            })
        };

        $scope.setCompleted = function() {
            var status = typeof skill.score.state == "undefined" ? true : !skill.score.state;
            console.log('setting completed');
            console.log(skill);
            Skill.setCompleted(skill.id,status).then(function(data){
                console.log('received result');
                console.log(data);
                skill.score.state = data.status;
            })
        };

        $scope.getCompletionMessage = function() {
            return "Set skill as " + (skill.score.state ? "not ":"") + "completed";
        };

        $scope.getSuggestion = function() {
            return Suggester.suggest(skill);
        };

        $rootScope.application.pageTitle = 'Skill ' + skillId;

     }]);
