'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MyJobCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('MatchCtrl', ['$rootScope','$scope', 'Matcher', function ($rootScope, $scope, Matcher) {
        $rootScope.application.showMenu = false;
        $scope.profile = $rootScope.digimaps.profile;
        $rootScope.application.pageTitle = 'My matches';

        var loadData = function() {
            $scope.tasks = Matcher.getSelectedTasks();
            $scope.matches = Matcher.getMatches(3);
        };
        $scope.removeTask = function(task) {
            Matcher.removeSelectedTask(task);
            loadData();
        };

        loadData();
    }]);
