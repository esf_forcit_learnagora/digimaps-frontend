'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
    .controller('ChangePasswordCtrl', ['$scope', '$rootScope', '$location', '$cookies', 'dmConfiguration', 'Profile', 'Job', function ($scope, $rootScope, $location, $cookies, dmConfiguration, Profile, Job) {


        $rootScope.application.showMenu = false;
        $scope.logo = dmConfiguration.logo.main;

        $scope.message = "";

        $scope.update = function(credentials) {
            if (credentials.password == "" || credentials.password2 == "") {
                $scope.message = "field cannot be empty";
            } else if (credentials.password != credentials.password2) {
                $scope.message = "passwords are not identical";
            } else {
                Profile.changePassword(credentials.password).then(function(profile) {
                    $location.url('/changed');
                })

            }
        };

        $scope.proceed = function() {
            var stepInJob = $cookies.get('stepinjob');
            if (typeof stepInJob === 'undefined') {
                if ($rootScope.digimaps.profile.goals.length) {
                    $location.url('/dashboard');
                } else {
                    $location.url('/');
                }
            } else {
                Job.selectById(stepInJob).then(function() {
                    $cookies.remove('stepinjob');
                    $location.url('/dashboard');
                });
            }
        }

}]);
