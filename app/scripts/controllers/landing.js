'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MyJobCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */

angular.module('digimapsApp')
    .run(['$anchorScroll', function($anchorScroll) {
        $anchorScroll.yOffset = 50;   // always scroll by 50 extra pixels
    }])
    .controller('LandingCtrl', ['$anchorScroll', '$location', '$scope', '$rootScope',
        function($anchorScroll, $location, $scope, $rootScope) {

            $rootScope.application.showMenu = false;
            $scope.digimaps = $rootScope.digimaps;

            $scope.gotoAnchor = function(x) {
                var newHash = 'anchor' + x;
                if ($location.hash() !== newHash) {
                    // set the $location.hash to `newHash` and
                    // $anchorScroll will automatically scroll to it
                    $location.hash('anchor' + x);
                } else {
                    // call $anchorScroll() explicitly,
                    // since $location.hash hasn't changed
                    $anchorScroll();
                }
            };
        }

    ]);




