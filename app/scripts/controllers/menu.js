'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MyJobCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('MenuCtrl', ['$rootScope','$scope', 'Profile', function ($rootScope, $scope, Profile) {

        $scope.matchData = $rootScope.matches;
        $scope.digimaps = $rootScope.digimaps;

        if ($scope.digimaps.isAuthenticated && typeof $scope.digimaps.profile === 'undefined') {
            Profile.load().then( function(response) {
                $rootScope.digimaps.profile = response.profile
            });
        }

    }]);
