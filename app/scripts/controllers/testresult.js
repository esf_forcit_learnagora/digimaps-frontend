'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:TestJobResultCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
    .run(['$anchorScroll', function($anchorScroll) {
        $anchorScroll.yOffset = 0;
    }])
    .controller('TestResultCtrl', ['$anchorScroll', '$scope', '$rootScope', '$routeParams', 'Skill', 'Profile' , function ($anchorScroll, $scope, $rootScope, $routeParams, Skill, Profile) {

        $scope.profile = $rootScope.digimaps.profile;
        $rootScope.application.showMenu = false;

        var skillId = $routeParams.id;

        Skill.getScore(skillId).then(function(response) {
            $scope.lastTest = response.score;
            console.log($scope.lastTest.score);
            $scope.answers = response.answers;
            console.log(response);
        });

        Profile.load().then(function(profile) {
            var skill = null;
            _.each(profile.goals, function(goal) {
                var jobSkill = _.find(goal.job.job_skills, function(jobSkill) {
                    return jobSkill.skill.id == skillId;
                });
                if (jobSkill) { skill = jobSkill.skill}
                console.log(jobSkill);
            });
            if (skill) {
                $scope.skill = skill;
            }
        });

    }]);




