'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('TestCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'Skill' , function ($scope, $rootScope, $routeParams, $location, Skill) {

        console.log('test job controller started');

    	var currentId = $routeParams.id;
    	console.log(currentId);

        var numQuestionsPerTest = 10;


         $rootScope.application.pageTitle = 'Test skills';

        Skill.find(currentId).then( function(skill) {
                console.log(skill);
                $rootScope.application.pageTitle = "Test skill '"+skill.name + "'";
            }
        );
        var questions = [];
        Skill.getQuestions(currentId).then( function(q) {
                questions = q;
                questions = shuffle(questions);
                $scope.currentQuestion = 1;
                $scope.maxQuestions = Math.min(numQuestionsPerTest,questions.length);
                $scope.question = questions[$scope.currentQuestion-1];
                console.log('first question:');
                console.log($scope.question);
            }
        );



        $scope.nextQuestion = function(option) {
            if (option !== 'undefined') {
                Skill.answer($scope.question.id, option.id).then( function(q) {
                    $scope.currentQuestion++;
                    if ($scope.currentQuestion > $scope.maxQuestions) {
                        $rootScope.testIsFinished = true;
                        $location.path('/test/' + currentId + '/result');
                    }
                    $scope.question = questions[$scope.currentQuestion-1];
                })
            }
        };

        function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;
            // While there remain elements to shuffle...
            while (0 !== currentIndex) {
                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex -= 1;
                // And swap it with the current element.
                temporaryValue = array[currentIndex];
                array[currentIndex] = array[randomIndex];
                array[randomIndex] = temporaryValue;
            }
            return array;
        }

    }]);
