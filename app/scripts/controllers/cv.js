'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
    .controller('CvCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

        $rootScope.application.showMenu = false;

    }]);
