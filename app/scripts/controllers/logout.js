'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
    .controller('LogoutCtrl', ['$rootScope', '$location', 'OAuthToken', function ($rootScope, $location, OAuthToken) {

        OAuthToken.removeToken();
        $rootScope.digimaps.isAuthenticated = false;
        delete $rootScope.digimaps.profile;
        $location.url('/');
        console.log('logout finished');

    }]);
