'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:OrientationCtrl
 * @description
 * # OrientationCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('OrientationCtrl', ['$rootScope','$scope', '$location', 'Job', 'Matcher', function ($rootScope, $scope, $location, Job, Matcher) {
        $rootScope.application.showMenu = false;

        Matcher.isReady().then(function() {
            $scope.task = Matcher.getSingleTask();
            $scope.match = Matcher.getBestMatch();
        });
        
        var init = function() {
            $scope.task = null;
            $rootScope.numberOfSelections = typeof $rootScope.numberOfSelections === "undefined" ? 1 : $rootScope.numberOfSelections;
            $scope.numberOfSelections = $rootScope.numberOfSelections;
            $scope.match = null;
            $scope.instruct = $rootScope.instruct;
        };

        $scope.selectTask = function(task) {
            Matcher.addSelectedTask(task);
            $rootScope.numberOfSelections++;
            $scope.numberOfSelections = $rootScope.numberOfSelections;
            if ($scope.numberOfSelections>5) {
                $rootScope.numberOfSelections = 1;
                console.log("will go to match page");
                $location.path('/match');
            }
            $scope.task = Matcher.getSingleTask();
            $scope.match = Matcher.getBestMatch();
        };

        $scope.rejectTask = function(task) {
            $scope.task = Matcher.getSingleTask();
            $scope.match = Matcher.getBestMatch();
        };

        $scope.hideInstruct = function() {
            $rootScope.instruct = false;
            $scope.instruct = false;
        };

        init();

    }]);
