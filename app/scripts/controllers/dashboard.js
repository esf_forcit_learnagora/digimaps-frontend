'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:DashboardCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */

angular.module('digimapsApp')
    .controller('DashboardCtrl', ['$rootScope', '$scope', 'Profile', 'Job', 'Skill', function ($rootScope, $scope, Profile, Job, Skill) {

        $rootScope.application.showMenu = false;
        $scope.skills = []; //getSkills(profile.goals)

        var updateLayout = function() {
            console.log('will update layout');
            setTimeout(function() {
                var container = $('.grid');
                container.imagesLoaded( function() {
                    console.log('images are loaded');
                    container.masonry({
                        itemSelector : '.board-job'
                    });
                });
            }, 100);

            setTimeout(function() {
                var container = $('.grid-skills');
                container.imagesLoaded( function() {
                    container.masonry({
                        itemSelector : '.skill'
                    });
                });
            }, 100);
        };

        var getSkills = function(goals) {
            var jobSkills = [];
            _.each(goals, function(goal) {
                _.each(goal.job.job_skills, function(jobSkill){
                    var js = _.find(jobSkills, function(js) { return js.skill.id == jobSkill.skill.id});
                    if (js) {
                        js.weight+= jobSkill.weight;
                    } else {
                        jobSkills.push(jobSkill);
                    }
                })
            });
            jobSkills = _.sortBy(jobSkills, function(jobSkill) { return -jobSkill.weight});
            var skills = [];
            _.each(jobSkills, function(jobSkill) { skills.push(jobSkill.skill)});
            return skills;
        };

        $scope.getSkills = function(goals) {
            return getSkills(goals);
        };

        $scope.getCompletionMessage = function(skill) {
            return "Set skill as " + (skill.score.state ? "not ":"") + "completed";
        };

        if ($rootScope.digimaps.profile) {
            $scope.profile = $rootScope.digimaps.profile;
            $scope.skills = getSkills($scope.profile.goals);
            updateLayout();
        }

        $rootScope.$on("dm:profileloaded", function(event, profile) {
            console.log('profile updated');
            $scope.profile = profile;
            $scope.skills = getSkills($scope.profile.goals);
            updateLayout();
        });

        $scope.filter = function(goals) {
            $scope.skills = getSkills(goals);
            updateLayout();
        };

        $scope.removeGoal = function(goal) {
            Job.removeGoal(goal);
        };


        $scope.getJobSkills = function(jobs) {
            if (!jobs.isArray) jobs = [jobs];
            var skills = [];
            _.each(jobs, function(job) {
                _.each(job.job_skills, function(jobSkill) {
                    if (!_.find(skills, function(skill) { return skill.id == jobSkill.skill.id  })) {
                        skills.push(jobSkill.skill);
                    }
                });
            });
            return skills;
        };

        $scope.skillsCompleted = function(skills) {
            var completed = 0;
            _.each(skills, function(skill) {
                if (skill.score.state == 1) {
                    completed++;
                }
            });
            return completed;
        };
        $scope.skillsCompletedPct = function(job) {
            var completed = $scope.skillsCompleted($scope.getJobSkills(job));
            var total = job.job_skills.length;
            return total ? completed/total : 0;
        };
        $scope.getScore = function(entity) {
            return _.find($scope.profile.scores, function(score) { return score.id == entity.id});
        };
        $scope.getResult = function(score) {
            return score.max_score ? score.score/score.max_score : 0;
        };

        $scope.setCompleted = function(skill) {
            var status = typeof skill.score.state == "undefined" ? true : !skill.score.state;
            console.log('setting completed');
            console.log(skill);
            Skill.setCompleted(skill.id,status).then(function(data){
                console.log('received result');
                console.log(data);
                skill.score.state = data.status;
            })
        };

        $scope.reset = function(skillId) {
            console.log('will reset');
            Skill.resetScore(skillId).then( function(response) {
                //$scope.lastTest = response.score;
                //$scope.answers = response.answers;
                Profile.load().then(function(profile) {
                    $scope.profile = profile;
                    $scope.skills = getSkills($scope.profile.goals);
                    updateLayout();
                });
            })
        };

    }]);
