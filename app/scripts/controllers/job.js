'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
  .controller('JobCtrl', ['$scope', '$rootScope', '$routeParams', '$location', '$cookies', 'Job', 'Matcher', function ($scope, $rootScope, $routeParams, $location, $cookies, Job, Matcher) {

    	var currentId = $routeParams.id;

        $rootScope.application.showMenu = true;

          $scope.showTasks = function () {
              $('.skills').hide();
              $('.tasks').show();
              $('#skillBtn').removeClass('active');
              $('#taskBtn').addClass('active');
          };

          $scope.showSkills = function () {
              $('.tasks').hide();
              $('.skills').show();
              $('#taskBtn').removeClass('active');
              $('#skillBtn').addClass('active');
          };

        Job.find(currentId).then( function(job) {

            setTimeout(function() {
                var container = $('.grid');

                container.imagesLoaded( function() {
                    container.masonry({
                        itemSelector : '.skill'
                    });
                });
            }, 50);

            $scope.job = job;
            $scope.jobSkills = _.sortBy(job.job_skills, function(jobSkill) { return -jobSkill.weight; });
            $scope.match = Matcher.getMatchForJobId(currentId);
            console.log($scope.job);
            $scope.selectedTasks = Matcher.getSelectedTasksForJob(job);
            
            $scope.unselectedTasks = Matcher.getNotSelectedTasksForJob(job);
            $rootScope.application.pageTitle = job.name;

        });

        $scope.stepInJob = function(job) {
            if ($rootScope.digimaps.isAuthenticated) {
                Job.select(job).then(function() {
                    $location.url('/dashboard');
                });
            } else {
                $cookies.put('stepinjob',job.id);
                $rootScope.application.loginMessage = 'You need to be logged in to continue with the application';
                $location.url('/login');
            }


        }
    }]);
