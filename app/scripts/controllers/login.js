'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the digimapsApp
 */
angular.module('digimapsApp')
    .controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$cookies', 'OAuth', 'dmConfiguration', 'Profile', 'Job', 'Register', function ($scope, $rootScope, $location, $cookies, OAuth, dmConfiguration, Profile, Job, Register) {


        $rootScope.application.showMenu = false;
        $scope.logo = dmConfiguration.logo.main;
        $scope.loginMessage = $rootScope.application.loginMessage;

        var login = function(credentials) {
            OAuth.getAccessToken({
                'username': credentials.username,
                'password': credentials.password
            })
            .then(function() {
                $rootScope.digimaps.isAuthenticated = true;
                $rootScope.application.loginMessage = '';
                Profile.load().then( function(profile) {
                    if (profile.change_password) {
                        console.log('need to change the password');
                        $location.url('/change-password');
                    } else {
                        var stepInJob = $cookies.get('stepinjob');
                        if (typeof stepInJob === 'undefined') {
                            if (profile.goals.length) {
                                $location.url('/dashboard');
                            } else {
                                $location.url('/');
                            }
                        } else {
                            Job.selectById(stepInJob).then(function() {
                                $cookies.remove('stepinjob');
                                $location.url('/dashboard');
                            });
                        }
                    }
                });


            })
            .catch(function() {
                $rootScope.digimaps.isAuthenticated = false;
                $scope.loginMessage = $rootScope.application.loginMessage;
            });
        };

        $scope.login = login;

        $scope.register = function(credentials) {
            credentials.username = credentials.email;
            Register.register(credentials).then(function() {
                login(credentials);
            }, function(message) {
                console.log('error: '+message);
                //@todo: show error message on register page (username already in use)
            });
        }
}]);
