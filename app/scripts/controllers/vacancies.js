'use strict';

/**
 * @ngdoc function
 * @name digimapsApp.controllers:MyJobCtrl
 * @description
 * # MainCtrl
 * Controller of the digimapsApp
 */

angular.module('digimapsApp')
    .controller('VacanciesCtrl', ['$rootScope', function ($rootScope) {

        $rootScope.application.showMenu = false;

    }]);
