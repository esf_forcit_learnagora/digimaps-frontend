'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Matcher', [ '$rootScope', '$q', 'Job', function ($rootScope, $q, Job) {
        console.log('matcher started');
        var jobs = [];
        var jobsLoaded = false;
        var currentJobs = [];
        var uniqueTasks = [];
        var selectedTasks = [];
        var offeredTasks = [];

        var isReady = function() {
            var deferred = $q.defer();
            if (jobsLoaded) {
                deferred.resolve(true);
            } else {
                Job.getJobs().then(function(jobs) {
                    setJobs(jobs);
                    deferred.resolve(true);
                });
            }
            return deferred.promise;
        };

        var setJobs = function(j) {
            jobs = j;
            currentJobs = [];
            _.each(j, function(job) {
                job.job_tasks = _.sortBy(job.job_tasks, function(jobTask) { return -jobTask.weight; });
                currentJobs.push(job);
            });

            uniqueTasks = [];
            _.each(jobs, function(job) {
                _.each(job.job_tasks, function(jobTask) {
                    var task = jobTask.task;
                    if (!_.find(uniqueTasks,function(uniqueTask) { return task.id==uniqueTask.id})) {
                        uniqueTasks.push(task);
                    }
                })
            });

            //calculate probabilities
            _.each(jobs, function(job) {
                job.probability = 0.2;
                _.each(job.job_tasks, function(jobTask) {
                    var task = jobTask.task;
                    var falsePositive = 0;
                    var numOtherJobs = 0;
                    _.each(jobs,function(job2) {
                        if (job.id != job2.id) {
                            var jt = _.find(job2.job_tasks, function(jobTask2) { return jobTask.task.id == jobTask2.task.id});
                            falsePositive += jt ? jt.weight/3 : 0.2;
                            numOtherJobs++;
                        }
                    });

                    task.probabilities = {
                        positive: jobTask.weight/3,
                        falsePositive: numOtherJobs ? falsePositive/numOtherJobs : 0
                    };
                })
            })
        };

        var getSelectedTasks = function() {
            return selectedTasks;
        };
        var getSelectedTasksForJob = function(job) {
            var jobTasks = [];
            _.each(job.job_tasks, function(jobTask) {
                if (_.find(selectedTasks, function(task) { return task.id==jobTask.task.id })) {
                    jobTasks.push(jobTask);
                }
            });
            return _.sortBy(jobTasks, function(jobTask) { return -jobTask.weight; });
        };
        var getNotSelectedTasksForJob = function(job) {
            var jobTasks = [];
            _.each(job.job_tasks, function(jobTask) {
                if (!_.find(selectedTasks, function(task) { return task.id==jobTask.task.id })) {
                    jobTasks.push(jobTask);
                }
            });
            return _.sortBy(jobTasks, function(jobTask) { return -jobTask.weight; });
        };
        var addSelectedTask = function(task) {
            selectedTasks.push(task);
        };
        var removeSelectedTask = function(task) {
            var index = _.findIndex(selectedTasks, function(t) { return t == task});
            if (index != -1) selectedTasks.splice(index,1);
        };

        var getProbability = function(job, task) {
            return task.probabilities.positive*job.probability / (task.probabilities.positive*job.probability + task.probabilities.falsePositive*(1-job.probability));
        };

        var getMatches = function(numMatches) {
            var matches = [];
            _.each(jobs, function(job) {
                job.probability = 0.2;
                //console.log('updating '+job.name+', probability='+job.probability);
                _.each(job.job_tasks, function(jobTask) {
                    var task = jobTask.task;
                    if (_.find(selectedTasks, function(selectedTask) { return selectedTask.id == task.id})) {
                        job.probability = getProbability(job, task);
                        //console.log('after task '+task.id+',  probability='+job.probability);
                    }
                });
                matches.push({job: job,score: job.probability});
            });
            matches = _.sortBy(_.filter(matches,function(match){ return match.score>0}),function(match) {return -match.score}).splice(0,numMatches);

            $rootScope.matches = matches;
            return matches
        };

        var getBestMatch = function() {
            var matches = getMatches(1);
            return matches.length ? matches[0] : null;
        };

        var getMatchForJobId = function(jobId) {
            var job = _.find(jobs, function(j) {return j.id == jobId});
            if (job) {
                return {job: job,score: job.probability};
            }
            return null;
        };

        var getTwoTasks = function() {
            var candidateTasks = [];
            _.each(uniqueTasks,function(task) {
                if (!_.find(offeredTasks, function(offeredTask) {
                        return offeredTask.id == task.id
                    })) {
                    candidateTasks.push(task);
                }
            });
            console.log('we have '+candidateTasks.length + ' candidate tasks');

            var bestMatch = getBestMatch();
            console.log('These: '+bestMatch.job.name + ' is the best match');
            var extremes = {'high': {value: 0, task: null},'low': {value: 1,task: null}};
            _.each(candidateTasks,function(task) {
                var jobTask = _.find(bestMatch.job.job_tasks, function(jt) {return jt.task.id==task.id});
                if (jobTask) {
                    var probability = getProbability(bestMatch.job, jobTask.task);
                    if (probability > extremes.high.value) {
                        extremes.high.value = probability;
                        extremes.high.task = task;
                    }
                    if (probability < extremes.low.value) {
                        extremes.low.value = probability;
                        extremes.low.task = task;
                    }
                }
            });
            console.log('best result for task '+extremes.high.task.id+ ': '+extremes.high.value);
            console.log('lowest result for task '+extremes.low.task.id+ ': '+extremes.low.value);
            _.each(jobs, function(job) {
                console.log(job.probability + ': '+job.name);
            });

            offeredTasks.push(extremes.high.task);
            offeredTasks.push(extremes.low.task);
            return [extremes.high.task, extremes.low.task];
        };

        var getSingleTask = function() {
            var candidateTasks = [];
            _.each(uniqueTasks,function(task) {
                if (!_.find(offeredTasks, function(offeredTask) {
                        return offeredTask.id == task.id
                    })) {
                    candidateTasks.push(task);
                }
            });
            console.log('we have '+candidateTasks.length + ' candidate tasks');

            var bestMatch = getBestMatch();
            console.log('These: '+bestMatch.job.name + ' is the best match');
            var extremes = {'high': {value: 0, task: null},'low': {value: 1,task: null}};
            _.each(candidateTasks,function(task) {
                var jobTask = _.find(bestMatch.job.job_tasks, function(jt) {return jt.task.id==task.id});
                if (jobTask) {
                    var probability = getProbability(bestMatch.job, jobTask.task);
                    if (probability > extremes.high.value) {
                        extremes.high.value = probability;
                        extremes.high.task = task;
                    }
                    if (probability < extremes.low.value) {
                        extremes.low.value = probability;
                        extremes.low.task = task;
                    }
                }
            });
            console.log('best result for task '+extremes.high.task.id+ ': '+extremes.high.value);
            console.log('lowest result for task '+extremes.low.task.id+ ': '+extremes.low.value);
            _.each(jobs, function(job) {
                console.log(job.probability + ': '+job.name);
            });

            offeredTasks.push(extremes.high.task);
            //offeredTasks.push(extremes.low.task);
            return extremes.high.task;
        };

        return {
            isReady: isReady,
            setJobs: setJobs,
            getSelectedTasks: getSelectedTasks,
            getSelectedTasksForJob: getSelectedTasksForJob,
            getNotSelectedTasksForJob: getNotSelectedTasksForJob,
            addSelectedTask: addSelectedTask,
            removeSelectedTask: removeSelectedTask,
            getMatches: getMatches,
            getBestMatch: getBestMatch,
            getMatchForJobId: getMatchForJobId,
            getTwoTasks: getTwoTasks,
            getSingleTask: getSingleTask
        }
    }]);
