'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Job', [ '$rootScope', '$resource', 'dmConfiguration', '$q', 'Profile', function ($rootScope, $resource, dmConfiguration, $q, Profile) {

        var jobs = [];
        var jobsLoaded = false;

        var jobsResource = $resource(dmConfiguration.api.rootUrl, {}, {
            all: {
                url: dmConfiguration.api.rootUrl + '/jobs',
                method: 'GET'
            },
            job: {
                url: dmConfiguration.api.rootUrl + '/job/:id',
                method: 'GET'
            },
            tasks: {
                url: dmConfiguration.api.rootUrl + '/job/:id/tasks',
                method: 'GET',
                params: { id: '@id' }
            },
            skills: {
                url: dmConfiguration.api.rootUrl + '/job/:id/skills',
                method: 'GET',
                params: { id: '@id' }
            },
            tasks_authenticated: {
                url: dmConfiguration.api.authUrl + '/job/:id/tasks',
                method: 'GET',
                params: { id: '@id' }
            },
            rateTask: {
                url: dmConfiguration.api.authUrl + '/ratetask/:jobId/:taskId/:rating',
                method: 'GET',
                params: {jobId: '@jobId', taskId: '@taskId', rating: '@rating' }
            },
            select: {
                url: dmConfiguration.api.authUrl + '/job/:id/select',
                method: 'GET',
                params: { id: '@id' }
            },
            unselect: {
                url: dmConfiguration.api.authUrl + '/goal/:id/unselect',
                method: 'GET',
                params: { id: '@id' }
            }
        });

        var rateTask = function(job, task, rating) {
            var deferred = $q.defer();
            jobsResource.rateTask({jobId: job.id, taskId: task.id, rating: rating},function( response){
                deferred.resolve(response.results);
            });
            return deferred.promise;
        };



        var find = function(id) {
            var deferred = $q.defer();
            var job = _.find(jobs, function(job){
                return job.id == id;
            });
            if (typeof job === 'undefined') {
                jobsResource.job({id:id}, function(result) {
                    deferred.resolve(result);
                }, function(result) {
                    deferred.reject(result);
                })
            } else {
                deferred.resolve(job);
            }
            return deferred.promise;
        };

        var findSkill = function(id) {
            return _.find(skills, function(skill) {return skill.id == id});
        };

        var select = function(job) {
            return selectById(job.id)
        };
        var selectById = function(jobId) {
            var deferred = $q.defer();
            jobsResource.select({id: jobId},function(){
                Profile.load().then( function(profile) { deferred.resolve(profile);} );
            });
            return deferred.promise;
        };

        var removeGoal = function(goal) {
            var deferred = $q.defer();
            jobsResource.unselect({id: goal.id},function(){
                Profile.load().then( function(profile) { deferred.resolve(profile);} );
            });
            return deferred.promise;
        };

        /** NEW */
        var getJobs = function() {
            var deferred = $q.defer();
            if (jobsLoaded) {
                deferred.resolve(jobs);
            } else {
                jobsResource.all(null,function(data){
                    jobs = [];
                    _.each(data.results, function(result) {
                        jobs.push(result);
                    });
                    jobsLoaded = true;
                    deferred.resolve(jobs);
                });
            }

            return deferred.promise;
        };
        var getTasks = function() {
            var deferred = $q.defer();
            if (jobsLoaded) {
                deferred.resolve(getTasksFromJobs());
            } else {
                getJobs().then(function() {
                    deferred.resolve(getTasksFromJobs());
                });
            }
            return deferred.promise;
        };
        var getTasksFromJobs = function(){
            var tasks = [];
            _.each(jobs, function(job) {
                _.each(job.tasks, function(jobTask) {
                    if (!_.find(tasks, function(task) {
                            return jobTask.id == task.id
                        } )) {
                        tasks.push(jobTask);
                    }
                })
            });
            return tasks;
        };

        return {
            getJobs: getJobs,
            getTasks: getTasks,

            rateTask: rateTask,
            find: find,
            findSkill: findSkill,
            select: select,
            selectById: selectById,
            removeGoal: removeGoal
        }
    }]);
