'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.dmOAuthInterceptor
 * @description
 * # dmOAuthInterceptor
 * Factory in the digimapsApp.
 */
angular.module('digimapsApp')
  .factory('dmOAuthInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    return {
      responseError: function(rejection) {
        if (401 === rejection.status && rejection.data && ('invalid_grant' === rejection.data.error || 'access_denied' === rejection.data.error)) {
          $rootScope.$emit('oauth:error', rejection);
        }
        return $q.reject(rejection);
      }
    }
  }]);
