'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Suggester', [ function () {
        console.log('suggester started');

        var passScore = 0.8;

        var suggest = function(skill) {
            var totalQuestions = skill.score.total_questions;
            var questionsAnswered = skill.score.max_score;
            var questionsCorrect = skill.score.score;

            var questionsLeft = totalQuestions-questionsAnswered;
            var score = questionsAnswered ? questionsCorrect/questionsAnswered : 0;
            var progress = totalQuestions ? questionsAnswered/totalQuestions : 1;

            //console.log(questionsCorrect+"-"+answeredQuestions+"-"+totalQuestions);

            if (!questionsAnswered && questionsLeft) {
                return "START";
            }

            if (score > 1 - (1-passScore)*progress) {
                return "MARK";
            }

            if (score < passScore*progress) {
                return "LEARN";
            }

            if (!questionsLeft) {
                return "LEARN";
            }

            return "MORE";
        };

        return {
            suggest: suggest
        }
    }]);
