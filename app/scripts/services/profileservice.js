'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Profile', [ '$rootScope', '$resource', 'dmConfiguration', '$q', function ($rootScope, $resource, dmConfiguration, $q) {


        var profileResource = $resource(dmConfiguration.api.rootUrl, {}, {
            load: {
                url: dmConfiguration.api.authUrl + '/profile',
                method: 'GET'
            },
            changePassword: {
                url: dmConfiguration.api.authUrl + '/profile/change',
                method: 'POST'
            }
        });

        var load = function() {
            var deferred = $q.defer();
            profileResource.load(null,function(data){
                console.log(data.profile);

                _.each(data.profile.goals, function(goal) {
                    goal.job.score = _.find(data.profile.scores,function(score) {return score.id == goal.job.id});
                    _.each(goal.job.job_skills, function(jobSkill) {
                        jobSkill.skill.score = _.find(data.profile.scores,function(score) {return score.id == jobSkill.skill.id});
                        _.each(jobSkill.skill.topics, function(topic) {
                            topic.score = _.find(data.profile.scores,function(score) {return score.id == topic.id});
                            _.each(topic.objectives, function(objective) {
                                objective.score = _.find(data.profile.scores,function(score) {return score.id == objective.id});
                                _.each(objective.learning_resources, function(learningResource) {
                                    learningResource.score = _.find(data.profile.scores,function(score) {return score.id == learningResource.id});
                                })
                            })
                        })
                    })
                });
                console.log(data.profile);

                $rootScope.digimaps.profile = data.profile;
                $rootScope.$broadcast("dm:profileloaded", data.profile);
                deferred.resolve(data.profile);
            });
            return deferred.promise;
        };

        var changePassword = function(password) {
            var deferred = $q.defer();
            profileResource.changePassword({password:password},function(data){
                console.log(data.profile);

                _.each(data.profile.goals, function(goal) {
                    goal.job.score = _.find(data.profile.scores,function(score) {return score.id == goal.job.id});
                    _.each(goal.job.job_skills, function(jobSkill) {
                        jobSkill.skill.score = _.find(data.profile.scores,function(score) {return score.id == jobSkill.skill.id});
                        _.each(jobSkill.skill.topics, function(topic) {
                            topic.score = _.find(data.profile.scores,function(score) {return score.id == topic.id});
                            _.each(topic.objectives, function(objective) {
                                objective.score = _.find(data.profile.scores,function(score) {return score.id == objective.id});
                                _.each(objective.learning_resources, function(learningResource) {
                                    learningResource.score = _.find(data.profile.scores,function(score) {return score.id == learningResource.id});
                                })
                            })
                        })
                    })
                });
                console.log(data.profile);

                $rootScope.digimaps.profile = data.profile;
                $rootScope.$broadcast("dm:profileloaded", data.profile);
                deferred.resolve(data.profile);
            });
            return deferred.promise;
        };

        return {
            load: load,
            changePassword: changePassword
        }
    }]);
