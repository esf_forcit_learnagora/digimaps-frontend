'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Skill', [ '$rootScope', '$resource', 'dmConfiguration', '$q', function ($rootScope, $resource, dmConfiguration, $q) {


        var skillResource = $resource(dmConfiguration.api.rootUrl, {}, {
            all: {
                url: dmConfiguration.api.rootUrl + '/skills',
                method: 'GET'
            },
            skill: {
                url: dmConfiguration.api.rootUrl + '/skill/:id',
                method: 'GET'
            },
            questions: {
                url: dmConfiguration.api.authUrl + '/skill/:id/questions',
                method: 'GET',
                params: { id: '@id' }
            },
            answer: {
                url: dmConfiguration.api.authUrl + '/question/:id/answer/:answerId',
                method: 'GET',
                params: { id: '@id', answerId: '@answerId' }
            },
            score: {
                url: dmConfiguration.api.authUrl + '/skill/:id/score',
                method: 'GET',
                params: { id: '@id' }
            },
            reset: {
                url: dmConfiguration.api.authUrl + '/skill/:id/reset',
                method: 'GET',
                params: { id: '@id' }
            },
            complete: {
                url: dmConfiguration.api.authUrl + '/skill/:id/complete/:status',
                method: 'GET',
                params: { id: '@id', status: '@status' }
            }
        });

        var find = function(id) {
            var deferred = $q.defer();
            skillResource.skill({id:id}, function(result) {
                console.log(result);
                deferred.resolve(result);
            }, function(result) {
                deferred.reject(result);
            });
            return deferred.promise;
        };

        var getQuestions = function(id) {
            var deferred = $q.defer();
            skillResource.questions({id:id}, function(response) {
                deferred.resolve(response.questions);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var answer = function(id, answerId) {
            var deferred = $q.defer();
            skillResource.answer({id:id, answerId: answerId}, function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var getScore = function(id) {
            var deferred = $q.defer();
            skillResource.score({id:id}, function(response) {
                console.log(response);
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var resetScore = function(id) {
            var deferred = $q.defer();
            skillResource.reset({id:id}, function(response) {
                console.log(response);
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var setCompleted = function(id, status) {
            status = status ? 1 : 0;
            var deferred = $q.defer();
            skillResource.complete({id:id, status: status}, function(response) {
                console.log(response);
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        return {
            find: find,
            getQuestions: getQuestions,
            answer: answer,
            getScore: getScore,
            resetScore: resetScore,
            setCompleted: setCompleted
        }
    }]);
