'use strict';

/**
 * @ngdoc service
 * @name digimapsApp.Job
 * @description
 * # jobService
 * Fetches a job.
 */
angular.module('digimapsApp')
    .service('Register', [ '$resource', '$q', 'dmConfiguration', function ($resource, $q, dmConfiguration) {

        var resource = $resource(dmConfiguration.api.rootUrl, {}, {
            register: {
                url: dmConfiguration.api.rootUrl + '/register',
                method: 'POST'
            }
        });

        var register = function(data) {

            var deferred = $q.defer();
            resource.register(data, function(data){
                deferred.resolve(null);
            }, function(data) {
                deferred.reject(data.data.result);
            });
            return deferred.promise;
        };



        return {
            register: register
        }
    }]);
