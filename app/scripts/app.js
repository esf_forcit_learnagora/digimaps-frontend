'use strict';

/**
 * @ngdoc overview
 * @name digimapsApp
 * @description
 * # digimapsApp
 *
 * Main module of the application.
 */
angular
    .module('digimapsApp', [
        'ngRoute',
        'ngDraggable',
        'ngResource',
        'ngCookies',
        'ngSanitize',
        'angular-oauth2',
        'angular-svg-round-progressbar'
    ])
    .config(function ($routeProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
        $routeProvider
            .when('/', {
                templateUrl: 'views/landing.html',
                controller: 'LandingCtrl'
            })
            .when('/login', {
                templateUrl: 'views/login.html',
                controller: 'LoginCtrl'
            })
            .when('/logout', {
                templateUrl: 'views/logout.html',
                controller: 'LogoutCtrl'
            })
            .when('/change-password', {
                templateUrl: 'views/change-password.html',
                controller: 'ChangePasswordCtrl'
            })
            .when('/changed', {
                templateUrl: 'views/changed.html',
                controller: 'ChangePasswordCtrl'
            })
            .when('/register', {
                templateUrl: 'views/register.html',
                controller: 'LoginCtrl'
            })
            .when('/match', {
                templateUrl: 'views/match.html',
                controller: 'MatchCtrl'
            })
            .when('/orientation', {
                templateUrl: 'views/orientation.html',
                controller: 'OrientationCtrl'
            })
            .when('/job/:id', {
                templateUrl: 'views/job.html',
                controller: 'JobCtrl'
            })
            .when('/dashboard', {
                templateUrl: 'views/dashboard.html',
                controller: 'DashboardCtrl'
            })
            .when('/skills', {
                templateUrl: 'views/skills.html',
                controller: 'DashboardCtrl'
            })
            .when('/skill/:id', {
                templateUrl: 'views/skill.html',
                controller: 'SkillCtrl'
            })
            .when('/test/:id', {
                templateUrl: 'views/question.html',
                controller: 'TestCtrl'
            })
            .when('/test/:id/result', {
                templateUrl: 'views/result.html',
                controller: 'TestResultCtrl'
            })
            .when('/cv', {
                templateUrl: 'views/cv.html',
                controller: 'CvCtrl'
            })
            .when('/cv-work', {
                templateUrl: 'views/cv-work.html',
                controller: 'CvCtrl'
            })
            .when('/cv-skills', {
                templateUrl: 'views/cv-skills.html',
                controller: 'CvCtrl'
            })
            .when('/vacancies', {
                templateUrl: 'views/vacancies.html',
                controller: 'VacanciesCtrl'
            })
            .when('/vacancies/:id', {
                templateUrl: 'views/vacancy.html',
                controller: 'VacanciesCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .config(['OAuthProvider', 'dmConfiguration', function(OAuthProvider, dmConfiguration) {
        OAuthProvider.configure({
            baseUrl: dmConfiguration.oAuth.url,
            clientId: dmConfiguration.oAuth.clientId,
            clientSecret: dmConfiguration.oAuth.clientSecret,
            grantPath: '/oauth/v2/token',
            revokePath: '/oauth/v2/revoke'
        });
    }])
    .config(['OAuthTokenProvider', function(OAuthTokenProvider) {
        OAuthTokenProvider.configure({
            name: 'dm-token',
            options: {
                secure: false
            }
        });
    }])
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('dmOAuthInterceptor');
    })
    .run(['$rootScope', '$location', 'OAuth', 'Profile', function($rootScope, $location, OAuth, Profile) {
        $rootScope.instruct = true;
        $rootScope.application = {
            showMenu: false,
            pageTitle: 'not set',
            loginMessage: ''
        };

        $rootScope.digimaps = {
            isAuthenticated : OAuth.isAuthenticated(),
            profile : null
        };

        if ($rootScope.digimaps.isAuthenticated) {
            Profile.load().then( function(profile) {
                if (profile.goals.length) {
                    $location.url('/dashboard');
                } else {
                    $location.url('/');
                }
            });
        }

        $rootScope.$on('oauth:error', function(event, rejection) {
            if (401 === rejection.status && 'access_denied' !== rejection.data.error) {
                return OAuth.getRefreshToken()
                    .then(function() {
                        $rootScope.digimaps.isAuthenticated = OAuth.isAuthenticated();
                    })
                    .catch(function() {
                        $rootScope.digimaps.isAuthenticated = false;
                        $rootScope.application.loginMessage = "username and password combination are not valid";
                        $location.url('/login?error_reason=' + rejection.data.error);
                    });
            }

            $rootScope.digimaps.isAuthenticated = false;
            $rootScope.application.loginMessage = "username and password combination are not valid";
            return $location.url('/login?error_reason=' + rejection.data.error);
        });

        $rootScope.$on("$routeChangeStart", function(event, next) {
            //var loginExemptions = ['/', '/login', '/match', '/job'];
            var loginRequired = ['/expert'];
            var nextPath = next.$$route ? next.$$route.originalPath : undefined;

            if ( !$rootScope.digimaps.isAuthenticated && _.contains(loginRequired, nextPath)) {
                return $location.url('/login');
            }
        });
    }]);
