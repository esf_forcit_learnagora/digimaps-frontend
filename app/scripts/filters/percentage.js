/**
 * Created by bart on 16.12.16.
 */
'use strict';

/**
 * @ngdoc filter
 * @name digimapsApp.filter:percentage
 * @function
 * @description
 * # heightClass
 * Filter in the digimapsApp.
 */
angular.module('digimapsApp')
    .filter('percentage', ['$filter', function ($filter) {
    return function (input, decimals) {
        return $filter('number')(input * 100, decimals) + '%';
    };
}]);