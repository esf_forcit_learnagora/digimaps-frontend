/**
 * Created by bart on 18.11.16.
 */
angular.module('digimapsApp')
    .directive('back', function() {

    return {
        restrict: 'E',
        template: '<a href="#/"><span class="pull-left"><i class="fa fa-angle-left" aria-hidden="true"></i></span></a>',
        scope: {
            back: '@back'
        },
        link: function(scope, element, attrs) {
            $(element[0]).on('click', function() {
                history.back();
                scope.$apply();
            });
            $(element[1]).on('click', function() {
                history.forward();
                scope.$apply();
            });
        }
    };
});