angular.module('digimapsApp').directive('backImg', function() {
    return function(scope, element, attrs){
        var url = attrs.backImg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-repeat': 'no-repeat',
            'background-size': 'cover'
        });
    };
});